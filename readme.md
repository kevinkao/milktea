## Installation

```
"license": "MIT",
"type": "project",
"repositories": [{
    "type": "vcs",
    "url": "https://gitlab.com/kevinkao/milktea.git"
}],
```

```
$ composer require kevinkao/milktea
```

### DistributedRandomItemGenerator

```php
use KevinKao\MilkTea\DistributedRandomItemGenerator;
```

```php
<?php
$drig = new DistributedRandomItemGenerator();
$drig->addItem(1, 0.2);
$drig->addItem(2, 0.3);
$drig->addItem(3, 0.5);
$random = $drig->getDistributedRandomItem();
```