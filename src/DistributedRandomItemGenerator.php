<?php

namespace KevinKao\MilkTea;

class DistributedRandomItemGenerator
{
    protected $distribution;
    protected $distSum;

    public function __construct()
    {
        $this->distribution = [];
        $this->distSum = 0;
    }

    public function addItem($key, $distribution)
    {
        if (isset($this->distribution[$key])) {
            $this->distSum -= $this->distribution[$key];
        }
        $this->distribution[$key] = $distribution;
        $this->distSum += $distribution;
    }

    public function getDistributedRandomItem()
    {
        $rand = mt_rand() / mt_getrandmax();
        $ratio = 1.0 / $this->distSum;
        $tempDist = 0;
        $dist = $rand / $ratio;

        foreach($this->distribution as  $k => $v) {
            $tempDist += $v;
            if ($dist <= $tempDist) {
                return $k;
            }
        }
        return null;
    }
}
